function f = midlevel_MEK_ERK_model_integrated_3(k,t,S0data_std,...
    S1data_std,S2data_std)

% Initial concentrations of S0, ES0, ES1, S1, S2, and E in experiment
x0 = [5;0;0;0;0;0.66]; % uM

[T,X] = ode45(@(t,y) midlevel_MEK_ERK_model_1(t,y,k),t,x0);

f = [X(:,1)./S0data_std; X(:,4)./S1data_std; X(:,5)./S2data_std];

end