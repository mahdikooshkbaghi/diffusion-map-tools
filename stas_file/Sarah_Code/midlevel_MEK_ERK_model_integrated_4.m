function f = midlevel_MEK_ERK_model_integrated_4(k,t)

% Initial concentrations of S0, ES0, ES1, S1, S2, and E in experiment
x0 = [5;0;0;0;0;0.66]; % uM

[T,X] = ode45(@(t,y) midlevel_MEK_ERK_model_1(t,y,k),t,x0);

f = [X(:,1); X(:,4); X(:,5); X(:,6)];
end