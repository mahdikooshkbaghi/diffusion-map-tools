function dxdt = midlevel_MEK_ERK_model_1(t,x,k)
% x(1) S0, x(2) is ES0, x(3) is ES1, x(4) is S1, x(5) is S2, x(6) is E

xdot = zeros(6,1);
k1 = k(1,1);
k2 = k(2,1);
k3 = k(3,1);
k4 = k(4,1);
k5 = k(5,1);
k6 = k(6,1);
xdot(1) = -k1*x(1)*x(6) + k2*x(2);
xdot(2) = k1*x(1)*x(6) - k2*x(2) - k3*x(2);
xdot(3) = k3*x(2) - k4*x(3) + k5*x(6)*x(4) - k6*x(3);
xdot(4) = k4*x(3) - k5*x(6)*x(4);
xdot(5) = k6*x(3);
xdot(6) = -k1*x(6)*x(1) + k2*x(2) + k4*x(3) - k5*x(6)*x(4) + k6*x(3);
dxdt = xdot;

end


