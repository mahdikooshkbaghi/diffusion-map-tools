% Below, I take a fit for the midlevel MEK ERK model and plot the
% transients for S0, S1, and S2

k1 = 0.71;% (uM*min)^-1
k2 = 19; % min^-1
k3 = 6700; % min^-1
k4 = 9200; % min^-1
k5 = 0.97;% (uM*min)^-1
k6 = 5200; % min^-1

k = [k1;k2;k3;k4;k5;k6];

Time = [0;0.5;2;3.25;3.75;5;10;20]; % min

S0_data = [5;4;2.6;1.93;1.69;1.32;0.68;0.29]; % uM
S0_std_err = [0;0.357027;0.329875;0.371826;0.326083;0.249884;0.215658;0.116807]; % uM

S1_data = [0;0.85;1.44;1.45;1.49;1.39;0.88;0.15]; % uM
S1_std_err = [0;0.371663;0.270802;0.255183;0.172089;0.090033;0.276567;0.088482]; % uM

S2_data = [0;0.15;0.96;1.62;1.83;2.29;3.44;4.55]; % uM
S2_std_err = [0;0.166516;0.157974;0.192871;0.201013;0.212827;0.467022;0.195826]; % uM

times = linspace(Time(1),Time(end));

all_fits = midlevel_MEK_ERK_model_integrated_4(k,times);

S0_fit = zeros(length(times),1);
S1_fit = zeros(length(times),1);
S2_fit = zeros(length(times),1);
E_fit = zeros(length(times),1);

for i = 1:length(times)
    n = all_fits(i,1);
    S0_fit(i,1) = n;
end

for i = 1:length(times)
    n = all_fits(length(times)+i,1);
    S1_fit(i,1) = n;
end

for i = 1:length(times)
    n = all_fits(2*length(times)+i,1);
    S2_fit(i,1) = n;
end

for i = 1:length(times)
    n = all_fits(3*length(times)+i,1);
    E_fit(i,1) = n;
end

%The following 3 plots show the fit through data points

figure(1)
plot(Time,S0_data,'ko',times,S0_fit,'b-')
hold on
errorbar(Time,S0_data,S0_std_err,'o')
legend('Data','Fitted ODE')
title('S0 Fit')
xlabel('Time (min)')
ylabel('Conc (uM)')

figure(2)
plot(Time,S1_data,'ko',times,S1_fit,'b-')
hold on
errorbar(Time,S1_data,S1_std_err,'o')
legend('Data','Fitted ODE')
title('S1 Fit')
xlabel('Time (min)')
ylabel('Conc (uM)')

figure(3)
plot(Time,S2_data,'ko',times,S2_fit,'b-')
hold on
errorbar(Time,S2_data,S2_std_err,'o')
legend('Data','Fitted ODE','Location','southeast')
title('S2 Fit')
xlabel('Time (min)')
ylabel('Conc (uM)')
