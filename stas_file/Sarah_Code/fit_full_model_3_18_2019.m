% We will sample and store 1000 parameter sets
% For all enzymes, the median kcat is 600 per minute
k1_lit = 1; % (min*uM)^-1
k2_lit = 10; % min^-1
k3_lit = 10; % min^-1
k4_lit = 10; % min^-1
k5_lit = 1; % (min*uM)^-1
k6_lit = 10; % min^-1

num_param_samples = 20;
k1_vec = zeros(num_param_samples,1);
k2_vec = zeros(num_param_samples,1);
k3_vec = zeros(num_param_samples,1);
k4_vec = zeros(num_param_samples,1);
k5_vec = zeros(num_param_samples,1);
k6_vec = zeros(num_param_samples,1);
err_vec = zeros(num_param_samples,1);

for j = 1:num_param_samples
    % First, pick starting values k1-k6 based on the literature
    k1_lb = k1_lit/1000; % (min*uM)^-1
    k1_ub = k1_lit*1000; % (min*uM)^-1
    k2_lb = k2_lit/1000; % min^-1
    k2_ub = k2_lit*1000; % min^-1
    k3_lb  = k3_lit/1000; % min^-1
    k3_ub = k3_lit*1000; % min^-1
    k4_lb = k4_lit/1000; % (min*uM)^-1
    k4_ub = k4_lit*1000; % (min*uM)^-1
    k5_lb = k5_lit/1000; % min^-1
    k5_ub = k5_lit*1000; % min^-1
    k6_lb = k6_lit/1000; % min^-1
    k6_ub = k6_lit*1000; % min^-1

    lb = [k1_lb;k2_lb;k3_lb;k4_lb;k5_lb;k6_lb];
    ub = [k1_ub;k2_ub;k3_ub;k4_ub;k5_ub;k6_ub];

    k1_guess = 10^(log10(k1_lb) + (log10(k1_ub)-log10(k1_lb))*rand);
    k2_guess = 10^(log10(k2_lb) + (log10(k2_ub)-log10(k2_lb))*rand);
    k3_guess = 10^(log10(k3_lb) + (log10(k3_ub)-log10(k3_lb))*rand);
    k4_guess = 10^(log10(k4_lb) + (log10(k4_ub)-log10(k4_lb))*rand);
    k5_guess = 10^(log10(k5_lb) + (log10(k5_ub)-log(k5_lb))*rand);
    k6_guess = 10^(log10(k6_lb) + (log10(k6_ub)-log(k6_lb))*rand);

    k_guess = [k1_guess;k2_guess;k3_guess;k4_guess;k5_guess;k6_guess];

    Time = [0.5;2;3.25;3.75;5;10;20]; % min
  
    % Data means and standard errors (minus the initial point)
    % This is the wild type data from 11-30-18
    % The standard deviation of the mean for the last entry of S1 is
    % fabricated
    S0data_mean = [4.07;2.07;1.14;0.9;0.6;0.24;0.08]; % uM
    S0data_std = [0.25;0.39;0.34;0.2;0.13;0.04;0.04]; % uM
    
    S1data_mean = [0.85;1.63;1.81;1.55;1.57;0.97;0]; % uM
    S1data_std = [0.21;0.3;0.24;0.19;0.15;0.28;0.04]; % uM
    
    S2data_mean = [0.08;1.3;2.05;2.55;2.83;3.8;4.92]; % uM
    S2data_std = [0.09;0.17;0.25;0.19;0.26;0.28;0.04]; % uM

    S0data = S0data_mean./S0data_std; % no units

    S1data = S1data_mean./S1data_std; % no units

    S2data = S2data_mean./S2data_std; % no units

    Sdata = [S0data;S1data;S2data];

    [k,resnorm,residual,exitflag,output] = ...
        lsqcurvefit(@(k,t) midlevel_MEK_ERK_model_integrated_3(k,t,...
        S0data_std,S1data_std,S2data_std),k_guess,Time,Sdata,...
        lb,ub);

    num_time_points = length(Time);
    num_species = 3;
    num_param = 6;

    k1 = k(1,1); % (uM*min)^-1
    k2 = k(2,1); % min^-1
    k3 = k(3,1); % min^-1
    k4 = k(4,1); % min^-1
    k5 = k(5,1); % (uM*min)^-1
    k6 = k(6,1); % min^-1
    
    k1_vec(j,1) = k1;
    k2_vec(j,1) = k2;
    k3_vec(j,1) = k3;
    k4_vec(j,1) = k4;
    k5_vec(j,1) = k5;
    k6_vec(j,1) = k6;
    err_vec(j,1) = resnorm;
end

% Design color spectrum for error
max_err = max(err_vec);
min_err = min(err_vec);
dif_err = max_err - min_err;
err_normalized = (err_vec - min_err*ones(length(err_vec),1))/dif_err;
length_err_spectrum = 1000;
length_half_err_spectrum = length_err_spectrum/2;
red = [1, 0, 0];
magenta = [1, 0, 1];
blue = [0, 0, 1];
err_colors_good_half = [linspace(blue(1),magenta(1),length_half_err_spectrum)',...
    linspace(blue(2),magenta(2),length_half_err_spectrum)',...
    linspace(blue(3),magenta(3),length_half_err_spectrum)'];
err_colors_bad_half = [linspace(magenta(1),red(1),length_half_err_spectrum)',...
    linspace(magenta(2),red(2),length_half_err_spectrum)',...
    linspace(magenta(3),red(3),length_half_err_spectrum)'];
err_colors = [err_colors_good_half;err_colors_bad_half];

% Discretize error vector and assign colors
err_color_vec = zeros(length(err_vec),3);
err_discrete_scale = 0:1/(length_err_spectrum-1):1;
for i = 1:length(err_vec)
    for j = 1:length_err_spectrum
        discrete_error = err_discrete_scale(j);
        continuous_error = err_normalized(i);
        if discrete_error >= continuous_error
            color = err_colors(j,:);
            break
        end
    end
    err_color_vec(i,:) = color;
end


% Make 45 degree line for kon plot
x1 = log10(k1_lb);
x2 = log10(k5_ub);
y1 = log10(k1_lb);
y2 = log10(k5_ub);

figure(1)
plot([x1,x2],[y1,y2])
hold on
scatter(log10(k1_vec),log10(k5_vec),8,err_color_vec,'filled')
xlabel('log10(k1) (min*uM)^-1')
ylabel('log10(k5)(min*uM)^-1')
axis([log10(k1_lb) log10(k5_ub) log10(k1_lb) log10(k5_ub)])
c = colorbar;
c.TicksMode = 'manual';
c.TickLabelsMode = 'manual';
c.Ticks = [0,0.5,1];
c.TickLabelsMode = 'manual';
c.TickLabels = {num2str(min_err),num2str(min_err+(max_err-min_err)/2),...
    num2str(max_err)};
colormap(err_colors)

% Make 45 degree line for koff plot
x1 = log10(k2_lb);
x2 = log10(k4_ub);
y1 = log10(k2_lb);
y2 = log10(k4_ub);

figure(2)
plot([x1,x2],[y1,y2])
hold on
scatter(log10(k2_vec),log10(k4_vec),8,err_color_vec,'filled')
xlabel('log10(k2) min^-1')
ylabel('log10(k4) min^-1')
axis([log10(k2_lb) log10(k4_ub) log10(k2_lb) log10(k4_ub)])
c = colorbar;
c.TicksMode = 'manual';
c.TickLabelsMode = 'manual';
c.Ticks = [0,0.5,1];
c.TickLabelsMode = 'manual';
c.TickLabels = {num2str(min_err),num2str(min_err+(max_err-min_err)/2),...
    num2str(max_err)};
colormap(err_colors)

% Make 45 degree line for kcat plot
x1 = log10(k3_lb);
x2 = log10(k6_ub);
y1 = log10(k3_lb);
y2 = log10(k6_ub);

figure(3)
plot([x1,x2],[y1,y2])
hold on
scatter(log10(k3_vec),log10(k6_vec),[8],err_color_vec,'filled')
xlabel('log10(k3) min^-1')
ylabel('log10(k6) min^-1')
axis([log10(k3_lb) log10(k6_ub) log10(k3_lb) log10(k6_ub)])
c = colorbar;
c.TicksMode = 'manual';
c.TickLabelsMode = 'manual';
c.Ticks = [0,0.5,1];
c.TickLabelsMode = 'manual';
c.TickLabels = {num2str(min_err),num2str(min_err+(max_err-min_err)/2),...
    num2str(max_err)};
colormap(err_colors)

