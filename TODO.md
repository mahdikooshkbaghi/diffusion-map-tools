- In nearest neighb what should we put for the distances more than our neighbours?
- [ ] MPI Eigensolver: check [SLEPc for python](http://slepc.upv.es/slepc4py-current/docs/usrman/index.html) 

- [ ] use MKL threading:

```python
import ctypes
mkl_rt = ctypes.CDLL('libmkl_rt.so')
mkl_get_max_threads = mkl_rt.mkl_get_max_threads
mkl_rt.mkl_set_num_threads(ctypes.byref(ctypes.c_int(16)))
p.set_num_threads(16)
```
