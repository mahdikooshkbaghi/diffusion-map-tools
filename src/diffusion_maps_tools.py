import numpy as np
import scipy
from scipy.spatial.distance import pdist, squareform
from scipy.spatial import cKDTree
from scipy.sparse import  spdiags, csr_matrix, diags, coo_matrix
from scipy.sparse import  SparseEfficiencyWarning
from typing import Optional, Dict, Tuple
from scipy.sparse.linalg import eigs
import warnings
warnings.simplefilter('ignore',SparseEfficiencyWarning)
try:
    import hnswlib
except ImportError:
    pass

###### Diffusion Map Library #############
class diffusion_maps_tools(object):
    """ 
    The single class for Diffusion Maps dimensionality reduction
    algorithm.


    """
    __author__  = "Mahdi Kooshkbaghi"
    __credits__ = "Mahdi Kooshkbaghi"
    __license__ = "GPL"
    __version__ = "1.0.0"
    __email__   = "mahdi.kooshkbaghi@gmail.com"

    def __init__ (self, **kwargs):
        self.__dict__.update(kwargs)
    # Mutual distance between data
    def distance(self,
                 points: np.array,
                 method: Optional[str] = 'pdist',
                 pdist_options:  Optional[Dict]=None,
                 kdtree_options: Optional[Dict]=None,
                 hnswtree_options: Optional[Dict]=None) \
                            -> scipy.sparse.csr_matrix:
        """ Finding the pairwise distances between points.
        
        Parameters
        ----------
        points : np.array
                 The dataset matrix in the numpy array format
                 with (n_elements, n_features) dimension.

        method : str, optional(default=pdist), ['pdist'|'kdtree'|'hnswtree']
                 The method to find the distances between
                 points.
                 
                 The available methods are.
                 
                 'pdist' : Pairwise distances using scipy built-in

                 
                 'kdtree' : The scipy.spatial.KDTree is built on
                            data points and then based on the 
                            cut_off value, the distances larger
                            than cut_off will be ignored (=0)
                            and the resultind distance matrix
                            will be sparse.
                 
                 'hnswtree': The fast approximate nearest 
                             neighbor search presented in
                             Malkov, Yu A., and D. A. Yashunin. 
                             "Efficient and robust approximate 
                             nearest neighbor search using 
                             Hierarchical Navigable Small 
                             World graphs."arXiv:1603.09320 (2016)"
                             In this method instead of cut_off distances
                             we will use the number of nearest
                             neighbors, k. For large data this method
                             is faster than other methods implemented (yet).

        pdist_options: dict, optional (default=None)
                       This dictionary controls the metric and
                       other options which should be pass to
                       pdist.
                       For more information visit scipy documentation.

                       The available options are

                       metric : str, optional (default='euclidean')
                       
        kdtree_options: dict, optional (default=None)
                        This dictionary controls the KDTree options.
                        For default=None, the leafsize=10 and
                        cut_off = np.inf (no cut_off)

                        The available options are
                        
                        leafsize : int, optional
                                   The number of points at 
                                   which the algorithm switches 
                                   over to brute-force.
                                   The defult is 10.
                        cut_off : float, optional
                                  Computes a distance matrix 
                                  between two KDTrees, 
                                  leaving as zero any 
                                  distance greater than cut_off.
                                  The default is np.inf.

        hnswtree_options: dict, ioptional (default=None)
                          This dictionary controls the hnswtree options.
                          For default=None,
                          ef_construction = 100, M = 16, metric = 'l2',
                          n_neighbors = 5% of total points

                          The available options are

                          ef_construction : int, optional
                                            controls index search 
                                            speed/build speed tradeoff.
                                            The defult is 100.

                          M : int, optional:
                              is tightly connected with internal 
                              dimensionality of the data. 
                              Strongly affects the memory consumption (~M)
                              Higher M leads to higher accuracy/run_time 
                              at fixed ef/efConstruction.
                              The defult is 16.
                          
                          metric: str, optional
                                  'l2', Squared L2, d = sum((Ai-Bi)^2)
                                  'ip', Inner product, d = 1.0 - sum(Ai\*Bi)
                                  'cosine', Cosine similarity.
                                  The defult metric is 'l2'

                          n_neighbors : integer
                                        number of neighbors to 
                                        consider for each point.
                                        The default is 5% of total points.
        
        Return
        ------
        __d : scipy.sparse.csr_matrix
              The (sparse) pairwise distance matrix in CSR format.

        """

        num_elements, dim = np.shape(points)
        print('  > Dataset: %i points in %i dimensions'%(num_elements,dim))

        print('  > Compute distances with {}'.format(method))

        if (method=='pdist'):
            if pdist_options is None:
               metric = 'euclidean'
               args = ()
               kwargs = {}
            else:
               if 'metric' in pdist_options.keys():
                    metric = pdist_options['metric']
               else:
                    metric = 'euclidean'
               if 'args' in pdist_options.keys():
                    args = pdist_options['args']
               else:
                    args = ()
               if 'kwargs' in pdist_options.keys():
                    kwargs = pdist_options['kwargs']
               else:
                    kwargs = {}

            __d = pdist(points, metric=metric, *args, **kwargs)
            __d = squareform(__d)
            __d = csr_matrix(__d)
            __d.setdiag(0)
            print('  --> Distance matrix is {:.4f}% dense.' \
                      .format(__d.nnz/np.prod(__d.shape)*100))

        if (method=='kdtree'):
            if kdtree_options is None:
                leafsize = 10
                cut_off = np.inf
            else:
                if 'cut_off' in kdtree_options.keys():
                    cut_off = kdtree_options['cut_off']
                else:
                    cut_off = np.inf
                if  'leafsize' in kdtree_options.keys():
                    leafsize = kdtree_options['leafsize']
                else:
                    leafsize = 10
            print('  --> distances greater than %E are neglected'%(cut_off))
            __kdtree =  cKDTree(points, leafsize=leafsize)
            __d =__kdtree.sparse_distance_matrix(__kdtree,
                                    max_distance=cut_off,
                                    output_type='coo_matrix')
            print('  --> Distance matrix is {:.4f}% dense.' \
                      .format(__d.nnz/np.prod(__d.shape)*100))
            # scipy original coo ro csr is slow
            # __d = __d.tocsr()
            # Using Juan function
            __d = self.coo_tocsr(__d)
          

        if (method=='hnswtree'):
            if hnswtree_options is None:
                ef_construction = 100
                M = 16
                metric = 'l2'
                n_neighbors = int(num_elements/20)
            else:

                if 'M' in hnswtree_options.keys():
                    M = hnswtree_options['M']
                else:
                    M = 16

                if  'ef_construction' in hnswtree_options.keys():
                    ef_construction = hnswtree_options['ef_construction']
                else:
                    ef_construction = 100

                if  'metric' in hnswtree_options.keys():
                    metric = hnswtree_options['metric']
                else:
                    metric = 'l2'

                if  'n_neighbors' in hnswtree_options.keys():
                    n_neighbors = hnswtree_options['n_neighbors']
                else:
                    n_neighbors = int(num_elements/20)
            print('  --> %1i nearest neigh. are considered'%(n_neighbors))
            p = hnswlib.Index(space=metric, dim=dim)
            p.init_index(max_elements=num_elements, 
                         ef_construction=ef_construction, 
                         M=M)
            p.set_ef(n_neighbors+1)
            p.add_items(points)
            labels, distances = p.knn_query(points, k=n_neighbors)           
            distances = np.sqrt(np.float64(distances))
            # Get the columns and rows indeces of non-zero elements
            # The following part is slow
            row_ind, col_ind = np.nonzero(distances)
            dis_data = distances[row_ind, col_ind]
            col_ind = labels[row_ind, col_ind]
            del distances
            __d = csr_matrix((dis_data,(row_ind, col_ind)),
                    shape=(num_elements,num_elements))
            __d.setdiag(0)
            print('  --> Distance matrix is {:.4f}% dense.' \
                      .format(__d.nnz/np.prod(__d.shape)*100))

        return __d
    #####################################################
    def kernel(self, 
               metric: scipy.sparse.csr_matrix,
               epsilon: float) \
                       -> scipy.sparse.csr_matrix:
        """ Compute Kernel function exp(-||.||^2/epsilon)
        
        Parameters
        ----------
        metric : scipy.sparse.csr_matrix
                 The distance (metric) function between points
                 in CSR format

        epsilon : float
                  The kernel width

        Return
        ------
        K : scipy.sparse.csr_matrix
            The kernel matrix
        
        Notes
        -----
        Since the metric is sparse matrix
        the elemntwise exponential is not
        sparse anymore exp(0)=1.
        Here we apply the trick to make the
        kernel look like sparse matrix as well

        """
        print('  > Compute Kernel')
        metric_data = metric.data
        Kernel = np.exp(-np.square(metric_data)/epsilon)       
        __K = metric._with_data(Kernel, copy=True)
        return __K

    def ker_normalize(self, 
                      kernel: scipy.sparse.csr_matrix,
                      alpha: Optional[float] = 1) \
                              -> scipy.sparse.csr_matrix:
        """ Compute normalized kernel 
        
        Parameters
        ----------
        kernel : scipy.sparse.csr_matrix
                 The kernel function in sparse CSR format

        alpha : float, optional
                The normaliztion factor it should be in [0,1].
                The defult value is alpha = 1.

        Return
        ------
        K : scipy.sparse.csr_matrix
            The normalized kernel matrix
        
        Notes
        -----
        See the normaliztion procedure in 
        1. S. S. Lafon, "Diffusion Maps and Geometric Harmonics" (2004),
        2. C. J. Dsilva et. al, "Nonlinear intrinsic variables 
        and state reconstruction in multiscale simulations", 
        J. Chem Phys. 139, 184109 (2013). (formula 7)

        """
 
        print('  > Compute Normalized Kernel with alpha=%f'%alpha)    
        if alpha > 0:
           invdiag = diags(1.0/(kernel.sum(axis=1).A.ravel())**alpha)
           kernel = invdiag @ kernel @ invdiag
           invdiag = diags(1/kernel.sum(axis=1).A.ravel())
           kernel = invdiag @ kernel
           return kernel
        else:
           invdiag = diags(1/kernel.sum(axis=1).A.ravel())
           kernel = invdiag @ kernel
           return kernel

    def eigensolver(self,
                    matrix: csr_matrix,
                    num_eigenpairs: Optional[int] = 5,
                    tol: Optional[float]=1e-3) \
                            -> np.array:
        """ Compute the largest eigenvalues and corresponding 
        eigenvectors.
        
        Parameters
        ----------
        matrix : scipy.sparse.csr_matrix
                 The (normalized) kernel function in sparse CSR format

        num_eigenpairs: int, Optional
                        The first num_eigenpairs largest eigenvectors are 
                        computed. The default is the first five one.

        tol : float, optional:
              The tolerence to find the eigenvalues. The smaller is better
              but it is slower. By default the tolerence is set to
              10^{-3} and one can check if reducing tolerence will change 
              the eigenvalues or not.

        Return
        ------
        ev : np.array
             The first num_eigenpairs eigenvalues.
        ew : np.array
             The real part of corresponding eigenvectors stored in columns.

        """
        print('  > Compute First %1i Eigenvectors'%(num_eigenpairs))

        # initial vector guess
        v0 = np.ones(matrix.shape[0])
        ew, ev = eigs(matrix, 
                      k=num_eigenpairs,
                      which='LM',
                      tol=tol,
                      v0=v0)
        ii = np.argsort(np.abs(ew))[::-1]
        return ew[ii], ev[:, ii].real


    def coo_tocsr(self, A: scipy.sparse.coo_matrix) \
                  -> scipy.sparse.csr_matrix:
        """Convert matrix to Compressed Sparse Row format, fast.
        
        This function is derived from the corresponding SciPy
        code but it avoids the sanity checks that slow 
        `scipy.sparse.coo_matrix.to_csr down`. 
        In particular, by not summing duplicates 
        we can attain important speed-ups
        for large matrices.


        Parameters
        ----------
        A : scipy.sparse.coo_matrix
            The matrix in coo format.

            The dataset matrix in the numpy array format
            with (n_elements, n_features) dimension.

        Returns
        -------
        A : scipy.sparse.csr_matrix
            The input array is overwritten with CSR format.


        Notes
        -----
        Author = Juan M. Bello-Rivas
        ref = https://github.com/jmbr/diffusion-maps


        """
        __author__ = "Juan M. Bello-Rivas"
        from scipy.sparse import csr_matrix
        if A.nnz == 0:
            return csr_matrix(A.shape, dtype=A.dtype)

        m, n = A.shape
        # Using 32-bit integer indices allows
        # for matrices of up to 2,147,483,647
        # non-zero entries.
        idx_dtype = np.int32
        row = A.row.astype(idx_dtype, copy=False)
        col = A.col.astype(idx_dtype, copy=False)

        indptr = np.empty(n+1, dtype=idx_dtype)
        indices = np.empty_like(row, dtype=idx_dtype)
        data = np.empty_like(A.data, dtype=A.dtype)

        scipy.sparse._sparsetools.coo_tocsr(m, n, A.nnz, row, col, A.data,
                                            indptr, indices, data)

        return csr_matrix((data, indices, indptr), shape=A.shape)
