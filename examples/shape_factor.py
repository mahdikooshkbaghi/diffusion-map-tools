
# coding: utf-8

# In[1]:


import csv
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import pandas as pd
import numpy as np

# In[2]:


filename = 'Dataset_3D_shape_factor_Two_cylinders.csv'
df = pd.read_csv(filename)
D          = df['D']
L1         = df['L1']
CYL1_x     = df['CYL1_x']
CYL1_y     = df['CYL1_y']
CYL1_z     = df['CYL1_z']
CYL1_theta = df['CYL1_theta']
CYL1_phi   = df['CYL1_phi']
L2         = df['L2']
CYL2_x     = df['CYL2_x']
CYL2_y     = df['CYL2_y']
CYL2_z     = df['CYL2_z']
CYL2_theta = df['CYL2_theta']
CYL2_phi   = df['CYL2_phi']
S          = df['S']


# In[5]:

print(np.shape(S))
plt.figure(figsize=(5,5))
ax = plt.subplot(111, projection='3d')
p = ax.scatter(L1, L2, CYL2_phi, c= S)
ax.set_xlabel(r'$L_1$')
ax.set_ylabel(r'$L_2$')
ax.set_zlabel(r'$CYL2^e_x$')
cbar = plt.colorbar(p)
cbar.set_label(r'$S$')
plt.show()
